"""

-----------------------
CompteSimple
-----------------------
+titulaire
+solde
-----------------------
+créditer
+débiter
+afficher solde
__init__
-----------------------

"""

class CompteSimple:

    def __init__(self, titulaire, solde):
        self.titulaire = titulaire
        self.__solde = solde
    
    def credit(self, montant):
        self.__solde += montant

    def debit(self, montant):
        self.__solde -= montant

    def verif_positif(self, montant):
        if montant < 0:
            raise ValueError('Impossible de créditer un montant négatif.')

    def __str__(self):
        return 'Le montant du compte de {} est de: {} euros'.format(self.titulaire, self.__solde)

    @property
    def solde(self):
        return self.__solde

class Personne:
    def __init__(self, nom):
        self.nom = nom
    
    def __str__(self):
        return self.nom

class Banque:
    def __init__(self, nom): 
        self.nom = nom
        self.comptes = []
    
    def ouvrir_compte(self, client, depot):
        nouveau_compte = CompteSimple(client, depot)
        self.comptes.append(nouveau_compte)
        return nouveau_compte

    def __str__(self):
        return 'Voici les comptes de {}: {}'.format(self.nom, self.comptes)

    def calculall(self):
        return sum(elements.self for elements in self.comptes)
    #----ou sinon----#:
    def calculall2(self):
        somme = 0
        for c in self.comptes:
            somme += c.solde
        return somme

    def preleverFrais(self, frais):
        for c in self.comptes:
            c.debit(frais)


class CompteCourant(CompteSimple):
    def __init__(self, client, depot, releve):
        super().__init__(client, depot)
        self.releve = []
    
    def releve_credit(self, montant):
        super().credit(montant)
        self.releve.append(montant)
    
    def releve_debit(self, montant):
        super().debit(montant)
        self.releve.append(-montant)

    #def printReleve():
        #for transaction

    