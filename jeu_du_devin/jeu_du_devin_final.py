#encoding: utf8

#----------Jeu du Devin----------#


#R0 Faire deviner au Joueur 2 (J2) un chiffre choisit au hasard par le Joueur 1 (J1)

import random 

#Fonction pour que J1 choisisse un chiffre
def input_int(msg='', *,  min = None, max = None):
    
    #lire un entier au clavier correspondant aux options spécifiées ou non spécifiées
    int_correct = False
    while not int_correct:

        answer = input(msg)
        
        try:
            answer = int(answer)
    
            #vérifier que la valeur entrée correspond aux options spécifiées
            if(min is not None and answer < min):
                print('trop petit')
            elif(max is not None and answer > max):
                print('trop grand')
            else:
                int_correct = True
                
        except ValueError:
            print('Un entier est attendu')

    return answer


print("""Bonjour et bienvenue dans le jeu du devin.
La règle est très simple :
Le joueur 1 choisit un chiffre entre 0 et 1000 et le Joueur 2 a 10 essais pour deviner le chiffre.
Vous êtes prêt(e) ?""")

#Reprendre une nouvelle partie
continuer = True
while continuer == True:

    #R1 Proposer à l'utilisateur d'être J1 ou J2
    choix_joueur = False
    while not choix_joueur:
        choix_joueur = input('Souhaitez-vous être Joueur 1 (appuyez sur 1) ou Joueur 2 (appuyez sur 2) ?: ')

        if choix_joueur not in '1 2':
            print('Je n\'ai pas compris')
            choix_joueur = False

        #R2 l'utilisateur est J1 :
        elif choix_joueur == '1':
            choix_joueur = True
            print('Vous êtes Joueur 1')


            # R2 Demander à l'utilisateur de choisir un chiffre
            chiffre = input_int('Choisissez un chiffre entre 0 et 1000 : ', min=0, max=1000) 
            
            nbre_max = 1000
            nbre_min = 0    

            for essai in range(1,11):
            
                if nbre_max == chiffre or nbre_min == chiffre : 
                    print('Vous êtes sûr que je n\'ai pas gagné ?')
                    break

                elif nbre_max != chiffre and nbre_min != chiffre :    
                    supposition = (nbre_min + nbre_max)//2

                    print('essai ', essai) #Indiquer le nombre d'essais  

                    #Demander à l'utilisateur si "supposition " est trop grand ('G','g'), trop petit ('P','p'), ou exacte ('T','t')
                    print('Je pense que le chiffre auquel vous pensez est', supposition, '. Est-ce que j\'ai raison ?') 

                    reponse_correct = False
                    while not reponse_correct:

                        reponse = input('Merci d\'entrer \n"g" si le chiffre est trop grand \n"p" si le chiffre est trop petit \n"t" si le chiffre est le bon: ')

                        if reponse not in 'g p t':
                            print('Je n\'ai pas compris.')
                        if reponse is 'g':
                            nbre_max = supposition
                            reponse_correct = True
                        elif reponse is 'p':
                            nbre_min = supposition
                            reponse_correct = True
                        elif reponse is 't':
                            reponse_correct = True
                            print('Youpi, j\'ai trouvé !')
          
                #arrêter la boucle en cas de bonne réponse ou d'essais maximum
                if reponse == 't':
                    break
                elif essai == 10:
                    print('Mince, j\'ai perdu. J\'espère que vous n\'avez pas triché ;)')


        #R3 l'utilisateur est J2 :
        elif choix_joueur == '2':
            choix_joueur = True
            print('Vous êtes Joueur 2')

            chiffre = random.randint(0,1000) #R2 Choisir un chiffre au hasard
            #print(chiffre)

            for essai in range(1,11):

                print('essai ', essai) #R3 Indiquer à l'utilisateur le nombre d'essais

                supposition = int(input('Devinez à quel chiffre je pense entre 0 et 1000 : '))   #R4 Demander à l'utilisateur de proposer un chiffre
                
                if supposition > 0 and supposition < 1000:

                    #J2 n'a pas trouvé le chiffre
                    if supposition != chiffre: 
                    
                        if supposition < chiffre:     #  le chiffre est trop petit
                            print('Raté ! Le chiffre est trop petit.')

                        elif supposition > chiffre:   # le chiffre est trop grand
                            print('Raté ! Le chiffre est trop grand.')
                
                    elif supposition == chiffre:    # J2 a trouvé le chiffre
                        print('Félicitation, vous avez trouvé !')
                        break
                    
                    else:
                        print('Vous avez perdu :(')

                elif supposition < 0:
                    print('Un nombre positif s \' vous plait')

                elif supposition > 1000:
                    print('J \'ai dit \"entre 0 et 1000\"')

                else:
                    print('Vous n\'avez même pas tapé un chiffre !')

    #Demander pour arrêter ou nouvelle partie     
    demande = False
    while not demande:
        continuer = input('Voulez-vous commencer une nouvelle partie ? (o/n): ').lower() #Proposer de relancer le programme
        
        if continuer == 'n':
            print('vous avez quitté le jeu')
            continuer = False
            demande = True
        elif continuer == 'o':
            continuer = True
            demande = True
        else:
            print('Je n\'ai pas compris')
            demande = False



