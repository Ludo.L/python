import os
import json

# gérer des exceptions avec la fonction "raise" sans qu'il n'y ait des conflits entre les exceptions
class LaboException(Exception):
    pass
# Personne déjà présente dans le bureau
class PresentException(LaboException):
    pass
# Personne inconnue
class InconnuException(LaboException):
    pass


# créer un laboratoire
def creer_lab():
    return dict()

# enregistrer l’arrivée d’une nouvelle personne.
def enregistrer_arrivee(laboratoire, nom, bureau):
    if nom in laboratoire:
        raise PresentException('Personne déjà inscrite')
    else:
        laboratoire[nom] = bureau
 
# enregistrer le départ d’une personne.
def enregistrer_depart(laboratoire, nom):
    if nom in laboratoire:
        del laboratoire[nom]
    else :
        raise InconnuException('Personne inconnue')

# corriger le nom de la personne (en cas de mauvaise orthographe, etc.) 
def changer_nom(laboratoire, ancien_nom, nouveau_nom):
    if ancien_nom in laboratoire:
        laboratoire[nouveau_nom] = laboratoire[ancien_nom]
        del laboratoire[ancien_nom]
    else:
        raise InconnuException('Personne inconnue')

# modifier le bureau occupé par une personne
def changer_bureau(laboratoire, nom, bureau):
    if nom in laboratoire:
        laboratoire[nom] = bureau
    else:
        raise InconnuException('Personne inconnue')

# savoir si une personne est membre du laboratoire
def trouver_membre(laboratoire, nom):
    if nom in laboratoire:
        print('{} fait parti(e) du laboratoire !'.format(nom))
    else :
        raise InconnuException('Personne inconnue')

# connaitre le bureau d’une personne
def trouver_bureau(laboratoire, nom):
    if nom in laboratoire:
        print('{} est dans le bureau {}'.format(nom, laboratoire[nom]))
    else :
        raise InconnuException('Personne inconnue')

# produire le listing de tous les personnels avec le bureau occupé
def imprimer_labo(laboratoire):
    for key, value in sorted(laboratoire.items()):
        print(key,'-',value)

# produire le listing de tous les bureaux
def takeSecond(elem):
    return elem[1]
def imprimer_bureaux(laboratoire):
    for key, value in sorted(laboratoire.items(), key=takeSecond):
        print(value,'-', key)

# sauver les informations du labo dans un fichier
def sauver_informations(laboratoire):
    sauvegarde = 'laboratoire.txt'
    with open(sauvegarde, 'w') as fichier:
        json.dump(laboratoire, fichier)

      
 
