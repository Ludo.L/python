from gestion_lab import *
from menu import *
import json


#------------- IHM UTILISATEUR -------------


def afficher_menu(menu):
    print("""
    -------------------------------------------------------------------------
    ---------------------------------MENU------------------------------------
    -------------------------------------------------------------------------
""")
    for key, value in menu.items():
        print(key,'-', value[0])
    
def selection(choix, menu):
    choix_correct = False
    while not choix_correct:
        try:
            choix = int(input('Merci de choisir une option: '))
            for key in menu.keys():
                choix_correct = key in menu.keys()
        except ValueError as e:
            print('Veuillez entrer un chiffre svp.')
    return choix
 
def traiter(choix, menu):
    print('Vous souhaitez {}'.format(menu[choix][0]))
    menu[choix][1](laboratoire)



if __name__ == "__main__":

    laboratoire = {}
    with open('laboratoire.txt', 'r') as fichier:
        laboratoire = json.load(fichier)

    choix = ''

    print("""
    -------------------------------------------------------------------------
    ----------Bienvenue dans le programme de gestion de laboratoire----------
    -------------------------------------------------------------------------
    """)


    while choix != 0:
        afficher_menu(menu)
        choix = selection(choix, menu)
        traiter(choix, menu)

        
