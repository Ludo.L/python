from gestion_lab import *
import re

bureau_pattern = re.compile("[A-C][0-3][0-2]\d")

def _arrivee(laboratoire):
    nom = input('Entrez le nom de la personne : ')
    bureau_correct = False
    while not bureau_correct:
        bureau = input('Entrez le bureau : ')
        if bureau_pattern.match(bureau) is not None:
            bureau_correct = True
        else:
            print('Merci de saisir un bureau existant')
    enregistrer_arrivee(laboratoire,nom,bureau)

def _depart(laboratoire):
    nom = input('Entrez le nom de la personne : ')
    enregistrer_depart(laboratoire, nom)

def _changement_nom(laboratoire):
    ancien_nom = input('Entrez le nom à changer: ')
    nouveau_nom = input('Entrez le nouveau nom: ')
    changer_nom(laboratoire, ancien_nom, nouveau_nom)

def _changement_bureau(laboratoire):
    nom = input('Entrez le nom de la personne: ')
    bureau = input('Entrez le nouveau bureau: ')
    bureau_correct = False
    while not bureau_correct:
        bureau = input('Entrez le bureau : ')
        if bureau_pattern.match(bureau) is not None:
            bureau_correct = True
        else:
            print('Merci de saisir un bureau existant')
    changer_bureau(laboratoire, nom, bureau)
 
def _identifier_membre(laboratoire):
    nom = input('Entrez le nom de la personne: ')
    trouver_membre(laboratoire, nom)

def _identifier_bureau(laboratoire):
    nom = input('Entrez le nom de la personne: ')
    trouver_bureau(laboratoire, nom)

def _sauverquitter(laboratoire):
    sauver_informations(laboratoire)
    print('----------Vous avez quitté----------')

def _imprimerpersonnel(laboratoire):
    imprimer_labo(laboratoire)

def _imprimerbureaux(laboratoire):
    imprimer_bureaux(laboratoire)
    

menu = {
    1: ('Enregistrer une arrivée', _arrivee),
    2: ('Enregistrer un départ', _depart),
    3: ('Corriger le nom de la personne (en cas de mauvaise orthographe, etc.)', _changement_nom),
    4: ('Modifier le bureau occupé par une personne', _changement_bureau),
    5: ('Savoir si une personne est membre du laboratoire', _identifier_membre),
    6: ('Connaitre le bureau d’une personne', _identifier_bureau),
    7: ('Produire le listing de tous les personnels', _imprimerpersonnel),
    8: ('Produire le listing de tous les bureaux', _imprimerbureaux),
    0: ('Sauver et quitter', _sauverquitter)
}
