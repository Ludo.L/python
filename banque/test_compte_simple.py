from compte_simple import *


client1 = Personne('Fred')
client2 = Personne('Ludo')

banque1 = Banque('Picsou Bank')

compte1 = banque1.ouvrir_compte(client1, 300)
compte2 = banque1.ouvrir_compte(client2, 20000)

print(str(compte1))
print(str(compte2))
compte1.credit(200)
compte2.credit(1000)
print(str(compte1))
print(str(compte2))
compte1.debit(100)
compte2.debit(500)
print(str(compte1))
print(str(compte2))

print(str(banque1)) 