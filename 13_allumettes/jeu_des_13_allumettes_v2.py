
from random import randint
from input_int import input_int

# imprimer les allumettes restantes
def print_allumettes(allumettes):
    for x in range(3):
        print(' '.join(allumettes)) 

# Demander le nom du joueur
nom_joueur = input('Quel est votre nom ? ')

# Reprendre une nouvelle partie
recommencer = 'o'
while recommencer == 'o':

    # Demander le niveau de l'ordinateur
    niveau = {'d':'distrait', 'n':'naif', 'r':'rapide', 'e':'expert'}
    niveau_ordinateur = None
    while niveau_ordinateur == None or (niveau_ordinateur != 'd' and niveau_ordinateur != 'n' and niveau_ordinateur != 'r' and niveau_ordinateur != 'e'):
        niveau_ordinateur = input('Niveau de l\'ordinateur (n)aif, (d)istrait, (r)apide ou (e)xpert ? ').lower()
    print('Mon niveau est {}'.format(niveau[niveau_ordinateur]))

    # Demander quel joueur commence
    joueur_commence = False
    choix_commencer = input('Est-ce que vous commencez (o/n) ? ').lower()
    if choix_commencer == 'o':
        joueur_commence = True

    # Imprimer les allumettes
    allumettes = ['|','|','|','|','|','  |','|','|','|','|','  |','|','|']
    print_allumettes(allumettes)

    # tours de jeu
    while len(allumettes) > 0:

        tour = None
        while tour == None:
                
            if len(allumettes) >= 3:
                nbre_max = 3 
            elif len(allumettes) < 3:
                nbre_max = len(allumettes)
            
            # tour de l'ordinateur
            if joueur_commence == False:
                if niveau_ordinateur == 'd':
                    tour = randint(1, 3)
                elif niveau_ordinateur == 'n' or niveau_ordinateur == 'r':
                    tour = randint(1, nbre_max)
                elif niveau_ordinateur == 'e':
                    if len(allumettes) > 4:
                        tour = randint(1, nbre_max)
                    elif len(allumettes) <= 4 and len(allumettes) != 1:
                        tour = len(allumettes) - 1
                    else:
                        tour = 1 

                print('Ordinateur, combien d\'allumettes prenez-vous ? ', tour)
                
                if tour > len(allumettes):
                    print('Il reste seulement {} allumette(s)'.format(nbre_max))
                    tour = None

                if tour != None:
                    del allumettes[-tour:]

                if tour != None and len(allumettes) != 0:
                    print_allumettes(allumettes)

            # tour de l'utilisateur
            elif joueur_commence == True:
                tour = input_int('{}, combien d\'allumettes prenez-vous ? '.format(nom_joueur), min=1, max=3)
                
                if tour > len(allumettes):
                    print('Il reste seulement {} allumette(s)'.format(nbre_max))
                    tour = None

                if tour != None:
                    del allumettes[-tour:]

                if tour != None and len(allumettes) != 0:
                    print_allumettes(allumettes)

            # changer de joueur
            if tour != None:
                joueur_commence = not joueur_commence    

    # Imprimer qui a gagné
    if joueur_commence :
        print('Bravo {}, vous avez gagné !'.format(nom_joueur))
    else:
        print('Ordinateur a gagné !')

    demande = False
    while not demande:
        recommencer = input('Voulez-vous commencer une nouvelle partie ? (o/n): ').lower() #Proposer de relancer le programme
        if recommencer == 'n':
            print('Merci d\'avoir joué {}.'.format(nom_joueur))
            demande = True
        elif recommencer == 'o':
            demande = True


